#include <iostream>
#include <fstream>

using namespace std;

class Rectangle {
private:
	int a;
	int b;
public:
	void GetA();
	void GetB();
	int RArea() { //������� ������� ��������������  
		return a*b;
	}
	int RPerimeter() { //������� �������� ��������������
		return (a + b) * 2;
	}
};

void Rectangle::GetA() {
	cout << "INPUT A - ";
	a = 5;
	cout << a;
}

void Rectangle::GetB() {
	cout << "INPUT B - ";
	b = 4;
	cout << b << endl;
}

int main() {
	ofstream out;
	Rectangle R1;
	R1.GetA(); //��������� �
	R1.GetB(); //��������� �
	cout << "Area: " << R1.RArea() << ", Perimeter: " << R1.RPerimeter() << endl; //����� ������
	out.open("./text.txt", ios_base::trunc);
	out << R1.RArea() << " " << R1.RPerimeter() << endl;
	out.close();
	system("pause");
	return 0;
}